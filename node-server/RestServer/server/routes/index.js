const express = require('express');

const app = express();

app.use(require('./usuario'));
app.use(require('./lista')); //catalogos:
app.use(require('./login'));
app.use(require('./account'));
app.use(require('./payday'));
app.use(require('./lifepoint'));



module.exports = app;