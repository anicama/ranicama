const express = require('express');
const _ = require('underscore');
const nodemailer = require('nodemailer');
const Usuario = require('../models/usuario');
const Account = require('../models/account');
const Movimiento = require('../models/movimiento');
const { verificaToken, verificaAdmin_Role } = require('../middlewares/autenticacion');
// const { verificaToken, verificaAdmin_Role } = require('../middlewares/autenticacion');

const app = express();
app.all('*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    //res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header("Access-Control-Allow-Headers", "*");

    next();
});


app.get('/accounts/:doi/:tipo',verificaToken, (req, res) => {
    let doi = req.params.doi;
    let tipo = req.params.tipo;
    console.log(req.params);
    Account.find({
            doi: doi,
            tipo_cuenta: tipo
        } )
        /*   .skip(desde)
           .limit(limite)*/
        .exec((err, cuentas) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    err: { message: err}
                });
            }
            console.log("pasa aqui");
            res.json({
                ok: true,
                cuentas: cuentas
            });


        });


});


app.post('/baja', verificaToken,function (req, res) {
   
    let topoc_='NONE';
    let origen = req.body.cuenta_origen;
    let destino = req.body.cuenta_destino;
    console.log(req.body.params);
    var c1,c2;
    var compra=3.20; var venta= 3.45;

    //CUENTA1:
    Account.find({  cuenta: origen } )  .exec((err, cuentas) => { 
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err: { message: err}
                });
            }
            c1=cuentas[0];
          
                //CUENTA2:
                Account.find({  cuenta: destino } )  .exec((err, cuentas2) => { 
                    if (err) {
                        return res.status(400).json({
                            ok: false,
                            err: { message: err}
                        });
                    }
                c2=cuentas2[0];
                console.info("Cuentas1."+c1);
                console.info("Cuentas2."+c2);

                //UPDATE 1:
                Account.findOneAndUpdate({  cuenta: origen}, { tipo_cuenta: 'LISTA99'}, (err, cc1) => { 
                    if (err) {
                        console.info("ERROR");
                        return res.status(400).json({ok: false, err: { message: "Error al actualzar Cuota"}});
                    }   
                    console.info("TERMINA UPDAT 1: "+cc1); 
                   
                }); 

                var new_saldo;
                if(c1.moneda==='PEN'&&c2.moneda==='USD'){
                    topoc_='pen_usd';
                        console.info('PEN ==> USD');
                        new_saldo=Number(c2.saldo)+Number(c1.saldo/venta);
                }else  
                if(c1.moneda==='USD'&&c2.moneda==='PEN'){
                        topoc_='usd_pen';
                         console.info('USD ==> PEN'); 
                            
                         new_saldo=Number(c2.saldo)+Number(c1.saldo*compra);
                }else {
                        console.info('SIN CONVERSION');
                        new_saldo=Number(c2.saldo)+Number(c1.saldo);
                } 

                new_saldo=parseFloat(new_saldo).toFixed(2)
                
                 //UPDATE 1:
                 Account.findOneAndUpdate({  cuenta: destino}, { saldo: new_saldo}, (err, cc2) => { 
                    if (err) {
                        console.info("ERROR");
                        return res.status(400).json({ok: false, err: { message: "Error al actualzar Cuota"}});
                    }   
                    console.info("TERMINA UPDAT 2 "+cc2);

                     //RESULTADO FINAL:
                        console.log("FIN METODO");
                        res.json({
                            ok: true,
                            msg: "Actualizados" 
                        }); 
                        
                        });

               

        }); 

   }); 
       
});


app.post('/movimiento',verificaToken, function (req, res) {
   
    let topoc_='NONE';
    let body = req.body;
    //VALIDAR PARAEMTROS
    if (body.doi==null    ||body.doi<=0  || body.doi == undefined)   { return res.status(200).json({ok: false, err: { message:  "el doi es requerido"}});}
    if (body.cuenta==null ||body.cuenta<=0  || body.cuenta == undefined){ return res.status(200).json({ok: false, err: { message:  "La cuenta es requerida"}});}
    if (body.monto==null  ||body.monto<=0  || body.monto == undefined) { return res.status(400).json({ok: false, err: { message:  "Ingrese un monto válido "}});}
    if (body.operacion==null||body.operacion<=0  || body.operacion == undefined) { return res.status(400).json({ok: false, err: { message:  "El tipo d eOperacione es requerida "}});}

    if (body.cuenta_abono==null||body.cuenta_abono<=0  || body.cuenta_abono == undefined) { return res.status(400).json({ok: false, err: { message:  "La cuenta de abono es requerida"}});}
    
    //REGLAS:
    if (body.cuenta ==  body.cuenta_abono) { return res.status(400).json({ok: false, err: { message:  "No puedes transferir a la misma cuenta "}});}
    
    let cuenta_origen;
    let cuenta_destino;
    console.info('BODY MONTO: '+parseFloat(body.monto).toFixed(2));
        //CUENTA DESTINO
        Account.find({  doi: body.doi,  cuenta: body.cuenta_abono  }, 'doi cuenta saldo tipo_cuenta tipo moneda')
        .exec((err, cuenta) => {
            if (err) {  return res.status(400).json({  ok: false, err  });} //SI HAY ALGUN ERROR 
            if (cuenta.length==0) { return res.status(200).json({ok: false, err: { message: "Cuenta no encontrada Abono"}});} // SI NO EXISTE LA CUENTA:
            cuenta_destino=cuenta[0];  

            console.info('CUENTA  DESTINO: '+cuenta_destino);
            //CUENTA ORIGEN
            Account.find({  doi: body.doi,  cuenta: body.cuenta  }, 'doi cuenta saldo tipo_cuenta tipo moneda')
            .exec((err, cuenta2) => {
                if (err) {  return res.status(400).json({  ok: false, err  });} //SI HAY ALGUN ERROR 
                if (cuenta2.length==0) { return res.status(200).json({ok: false, err: { message: "Cuenta no encontrada"}});} // SI NO EXISTE LA CUENTA:
                 cuenta_origen=cuenta2[0]; 
                
                 console.info('CUENTA ORIGEN: '+cuenta_origen);
                 console.info(body.monto);
                 console.info(cuenta_origen.saldo);
                 console.info(body.monto<=cuenta_origen.saldo);
                var min=body.monto,max=Number(cuenta_origen.saldo);
                console.info(min+ '<='+ max);
                if(min<=max) {  
                   
                        if(cuenta_origen.moneda==='PEN'&&cuenta_destino.moneda==='USD'){
                         topoc_='pen_usd';
                            console.info('PEN ==> USD');
                        }else  if(cuenta_origen.moneda==='USD'&&cuenta_destino.moneda==='PEN'){
                              topoc_='usd_pen';
                                console.info('USD ==> PEN');
                            
                        }else {
                            console.info('SIN CONVERSION');
                        }
 
                actualizarSaldoCuenta_(cuenta_origen,1,body,res,topoc_,cuenta_origen);
                actualizarSaldoCuenta_(cuenta_destino,0,body,res,topoc_,cuenta_origen)
                }else {
                    return res.status(400).json({
                        ok: false,
                       err: { message:  "No cuenta con fondos suficientes."}
                    });
                }
    
            });

        });

 
       
 
        




});

actualizarSaldoCuenta_ = (cuenta,tipo,body,res, opciontc,cuenta2)=> {
    var body_monto_tc;//CONVERTIDO AL TIPO DE CAMBIO;
    var compra=3.20; var venta= 3.45;
    var final=false; var logical; var cuenta_operacion; 

    if(tipo===1){ 
        cuenta.saldo = (parseFloat(cuenta.saldo)- parseFloat(body.monto)); //ACTUALIZAS EL SALDO 
        logical='- ';
        cuenta_operacion=body.cuenta + ' '+cuenta.moneda; 
        body_monto_tc=body.monto;
    }else if(tipo===0){ 
        final=true;
        if(opciontc==='pen_usd'){ 
            cuenta.saldo = parseFloat(cuenta.saldo)+(body.monto/venta); //ACTUALIZAS EL SALDO 
            body_monto_tc=parseFloat(body.monto/venta).toFixed(2); 
        }else if(opciontc==='usd_pen'){  
            cuenta.saldo =  parseFloat(cuenta.saldo)+(body.monto*compra); //ACTUALIZAS EL SALDO 
            body_monto_tc=parseFloat(body.monto*compra).toFixed(2);
        }else {
            cuenta.saldo = (parseFloat(cuenta.saldo)+ parseFloat(body.monto)); //ACTUALIZAS EL SALDO 
            body_monto_tc= body.monto;
        }
       
        logical='+ ';
        cuenta_operacion=body.cuenta_abono+ ' '+cuenta.moneda; 
    } 
    
    var date = new Date();
    var current_hour = (date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + ':' + date.getMilliseconds());

    cuenta.saldo = parseFloat( cuenta.saldo).toFixed(2);
    Account.findByIdAndUpdate(cuenta._id, cuenta, (err, cuentalast) => { 
        if (err) {
            return res.status(200).json({ok: false, err: { message: "Error al actualzar Saldo"+ tipo}});
        } 

        //REGISTRAR MOVIMIENTO:
        var movimiento = new Movimiento({
            cuenta: cuenta_operacion,
            canal: body.canal,
            doi: body.doi,
            operacion: body.operacion,
            monto: logical.concat(parseFloat(body_monto_tc).toFixed(2)) ,
            /*cuenta_abono: body.cuenta_abono,*/
            tipopago: 'TRANSF001',
            doi_abono: body.doi,
            fechahora: current_hour
        }); 
        registrarMovieento_(movimiento,res,final,body,body_monto_tc,cuentalast,cuenta2);
    })
}

registrarMovieento_ = (movimiento,res,final,body,body_monto_tc,c1,c2 ) => { 

    

    
    movimiento.save((err, movimientoDB) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err: { message: err}
            });
        }
        
        if(final){
            res.json({
                ok: true,
                movimiento: movimientoDB
            });
        }

        Usuario.findOne({ doi: movimiento.doi }, (err, usr) => {

            if (err) {
                console.log('usuario error:'+ err);
            }
    
            if (!usr) {
                console.log('usuario no existe');
                   
            }else {
                console.log('usuario  existe:' );
                if(final){
                
                   SendMail(usr.email, usr.nombre, movimiento.operacion, movimiento._id, movimiento.fechahora,c1.cuenta+' '+c1.tipo,c1.moneda +' '+  parseFloat(body.monto).toFixed(2), c2.cuenta+' '+c2.tipo,c2.moneda +' '+ body_monto_tc, usr.nombre);
                }
                
            }
        });
       

       
    });
}
app.get('/movimiento/:doi/:tipo',verificaToken, (req, res) => {
    let doi = req.params.doi;
    let tipopago = req.params.tipo;
     
    console.log(req.params);
    Movimiento.find({
            doi: doi,
            tipopago: tipopago
        }, 'canal fechahora monto operacion cuenta tipopago ').sort({ _id: -1 })
        /*   .skip(desde)
           .limit(limite)*/
        .exec((err, movimientos) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    err: { message: "Cuenta no encontrada"}
                });
            }
            console.log("pasa aqui");
            
            res.json({
                ok: true,
                movimientos: movimientos
            });


        });




});


SendMail = (to,titular_,tipo_,numero_,fecha_hora_,cuanta_cargo_,importe_,cuenta_abono_,importe_abono_,titular_abono_) => { 
    var transporter = nodemailer.createTransport({
        host: "hd-4918.banahosting.com",
        port: 465,
        secure: true, // true for 465, false for other ports
        auth: {
          user: "api-test@latam-server.com",
          pass: "bbva2019!"
        },logger: true,
        debug: true // include SMTP traffic in the logs
      });
      
      var body='<div><div class="adM">&nbsp;</div><center><table width="540px" cellspacing="0"><tbody><tr><td colspan="2" align="left"><img class="CToWUd" style="display: block; margin-left: auto; margin-right: auto;" src="https://s3-eu-west-1.amazonaws.com/rankia/images/valoraciones/0010/9848/Banco-BBVA-Continental.jpg" width="265" height="70" align="left" border="0" data-image-whitelisted=""/></td></tr><tr><td style="font-family: Verdana; color: #2f4181; width: 854px; font-size: 17px;" align="left"><p style="text-align: center;">&nbsp;<strong>Constancia de Operaci&oacute;n</strong></p></td></tr><tr><td colspan="2"><div style="width: 100%;">Tu operaci&oacute;n fue realizada con &eacute;xito.</div></td></tr><tr><td align="left"><table border="0" width="100%"><tbody><tr><td align="left" width="30%">&nbsp;</td></tr><tr><td align="left" width="30%">&nbsp;</td></tr><tr><td align="left">Titular:</td><td align="left">@TITULAR</td></tr><tr><td align="left">Tipo de operaci&oacute;n:</td><td align="left">@TIPO</td></tr><tr><td align="left">N&uacute;mero de operaci&oacute;n:</td><td align="left">@NUMERO</td></tr><tr><td align="left">Fecha y hora:</td><td align="left">@FECHA_HORA</td></tr><tr><td align="left" width="30%">&nbsp;</td></tr><tr><td align="left">Cuenta de cargo:</td><td align="left">@CUENTA_CARGO</td></tr><tr><td align="left">Importe transferido:</td><td align="left">@IMPORTE_TRANSFERIDO</td></tr><tr><td align="left">Importe cargado:</td><td align="left">@IMPORTE_CARGADO</td></tr><tr><td align="left" width="30%">&nbsp;</td></tr><tr><td align="left">Cuenta de abono:</td><td align="left">@CUENTA_ABONO</td></tr><tr><td align="left">Titular cuenta de abono:</td><td align="left">@TITULAR_ABONO</td></tr><tr><td align="left">Importe abonado:</td><td align="left">@IMPORTE_ABONADO</td></tr><tr><td align="left" width="30%">&nbsp;</td></tr><tr><td align="left">Referencia:</td><td align="left">@REFERENCIA</td></tr><tr><td align="left">&nbsp;</td><td align="left">&nbsp;</td></tr></tbody></table></td></tr><tr><td>&nbsp;Atentamente<div style="color: #2f4181; text-align: left; width: 65%;"><h4>&nbsp;BBVA Continental</h4></div></td></tr></tbody></table></center></div>';
      body=body.replace('@TITULAR',titular_);
      body=body.replace('@TIPO',tipo_);
      body=body.replace('@NUMERO',numero_);
      body=body.replace('@FECHA_HORA',fecha_hora_);
      body=body.replace('@CUENTA_CARGO',cuanta_cargo_);
      body=body.replace('@IMPORTE_TRANSFERIDO',importe_);
      body=body.replace('@IMPORTE_CARGADO',importe_);
      body=body.replace('@IMPORTE_ABONADO',importe_abono_);
      body=body.replace('@CUENTA_ABONO',cuenta_abono_);
      body=body.replace('@TITULAR_ABONO',titular_abono_);
      body=body.replace('@REFERENCIA',''); 
      
      var mailOptions = {
          from: '"BBA API " <api-test@latam-server.com>', // sender address
          to: to, // list of receivers
          subject: 'BBVA Continental - Constancia de Operación de tu Banca Por Internet   ', // Subject line
          //text: 'Hello world?', // plain text body
          html: body // html body
      };

      transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
            console.log('Message sent: %s', info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
        }
      });
}


app.get('/tipocambioo', (req, res) => {
    res.json({
        compra: '3.20',
        venta: '3.41'
    });

});
 
module.exports = app;