const mongoose = require('mongoose'),
autoIncrement = require('mongoose-auto-increment');
let Schema = mongoose.Schema;
autoIncrement.initialize(mongoose.connection);

let paydaySchema = new Schema({
    cuenta: {
        type: String 
    },
    letra: {
        type: String 
    }, 
    monto: {
        type: String 
    }, 
    diapago: {
        type: String 
    },
    estado: {
        type: Boolean 
    },
    disponible: {
        type: Boolean 
    },
    fechahora: {
        type: String ,
        default: new Date() 
    }
});
 
paydaySchema.methods.toJSON = function() { 
    let movim = this;
    let movimObject = movim.toObject(); 
    return movimObject;
}

 
paydaySchema.plugin(autoIncrement.plugin, 'PaydayAutoincrement');
module.exports = mongoose.model('Payday', paydaySchema);