const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

 

let Schema = mongoose.Schema;


let catalogoSchema = new Schema({
    codigo: {
        type: String,
        required: [true, 'El codigo es necesario']
    },
    nombre: {
        type: String,
        unique: true,
        required: [true, 'El nombre es necesario']
    },
    descripcion: {
        type: String,
        required: [true, 'La descripcion es necesaria']
    }, 
    estado: {
        type: Boolean,
        default: true
    } ,
    lista: {
        type: String,
        required: [true, 'la lista es necesaria']
    }
});


catalogoSchema.methods.toJSON = function() {

    let user = this;
    let userObject = user.toObject();
    //delete userObject.password;

    return userObject;
}


catalogoSchema.plugin(uniqueValidator, { message: '{PATH} debe de ser único' });


module.exports = mongoose.model('Catalogo', catalogoSchema);