const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

 

let Schema = mongoose.Schema;


let accountSchema = new Schema({
    cuenta: {
        type: String,
        required: [true, 'La cuenta es necesaria']
    },
    doi: {
        type: String, 
        required: [true, 'El documento de indentidad es necesario']
    },
    saldo: {
        type: String,
        required: [true, 'El saldo es necesario']
    }, 
    tipo_cuenta: {
        type: String,
        required: [true, 'El tipo de cuenta es necesario']
    } ,
    tipo: {
        type: String,
        required: [true, 'El tipo, o categoria es necesaria']
    }
    ,
    moneda: {
        type: String,
        required: [true, 'la moneda e snecesaria.']
    }
    
});


accountSchema.methods.toJSON = function() {

    let account = this;
    let accountObject = account.toObject(); 
    return accountObject;
}


accountSchema.plugin(uniqueValidator, { message: '{PATH} debe de ser único' });


module.exports = mongoose.model('Account', accountSchema);