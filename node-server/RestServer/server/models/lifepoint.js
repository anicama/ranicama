const mongoose = require('mongoose'),
autoIncrement = require('mongoose-auto-increment');
let Schema = mongoose.Schema;
autoIncrement.initialize(mongoose.connection);

let lifePointSchema = new Schema({
    doi: {
        type: String 
    },
    points: {
        type: String 
    }, 
    update: {
        type: String 
    }
});
 
lifePointSchema.methods.toJSON = function() { 
    let movim = this;
    let movimObject = movim.toObject(); 
    return movimObject;
}

 
lifePointSchema.plugin(autoIncrement.plugin, 'LifePontAutoincrement');
module.exports = mongoose.model('LifePoint', lifePointSchema);