// Import PolymerElement class
import {
    PolymerElement,
    html
} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '@polymer/iron-input/iron-input.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/app-route/app-location.js';
import '@vaadin/vaadin-checkbox/vaadin-checkbox.js';
import '@vaadin/vaadin-button/vaadin-button.js';
import '@vaadin/vaadin-icons/vaadin-icons.js';
import '@polymer/paper-fab/paper-fab.js';
import '@vaadin/vaadin-text-field/vaadin-password-field.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';
import '@vaadin/vaadin-notification/vaadin-notification.js';
import '@vaadin/vaadin-list-box/vaadin-list-box.js';
import '@vaadin/vaadin-select/vaadin-select.js';
import '@polymer/paper-card/paper-card.js';
import '@vaadin/vaadin-ordered-layout/vaadin-horizontal-layout.js';
import '@vaadin/vaadin-combo-box/vaadin-combo-box.js';
import '@vaadin/vaadin-grid/vaadin-grid.js';
import '@vaadin/vaadin-grid/vaadin-grid-column';
import '@vaadin/vaadin-grid/vaadin-grid-selection-column';
import '@polymer/iron-form/iron-form.js';
import '@vaadin/vaadin-radio-button/vaadin-radio-button.js';
import '@vaadin/vaadin-radio-button/vaadin-radio-group.js';
import '../shared-bootstrap.js';
import {
    MessageUtils,
    StringUtils,
    Navigation,
    WebService,
    WebSesion,
    WebAction,
    TipoMovimiento,
    Location,
    Token,
    Message,HTTPUtil
} from '../util/util-constants'; 
class AppCard extends PolymerElement {
    static get template() {
        return html `
 
<style include="shared-bootstrap"> 
 .first-letter {
    text-transform:uppercase;
}
</style>
<app-location route="{{route}}" url-space-regex="^[[rootPath]]">  </app-location>
<app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">  </app-route>
<iron-ajax  url="http://localhost:3000/accounts/[[user.doi]]/LISTA001"  handle-as="json" loading="{{cargando}}" id="ajaxCombo1"  on-response="responseCombo1" auto on-error="handleUserError"></iron-ajax>
<iron-ajax  url="http://localhost:3000/accounts/[[user.doi]]/LISTA011"  handle-as="json" loading="{{cargando}}" id="ajaxCombo2"  on-response="responseCombo2" last-response="responseCombo2" auto on-error="handleUserError"></iron-ajax>
<iron-ajax id="registrarPago" method="post" content-type="application/json" handle-as="text" on-response="responsePago" on-error="handleUserError"></iron-ajax>
<div class="py-10"></div>

<div class="container">
   <div class="jumbotron">
      <h3 class="display-5">Pago de Tarjetas de Crédito</h3>
      <p class="lead">En esta opción puede realizar el pago de sus tarjetas de crédito</p>
   </div>
   <div class="row marketing">
      <div class="col-lg-6">
         <h4>Seleccionar Cuenta</h4>
         <p>Cuenta de origen de fondos para realizar el pago.</p>
         <div>
            <vaadin-combo-box    id="combo1" style="width:95%" name="combo1"   item-value-path="cuenta"  item-label-path="cuenta">
            <template>
               
                <div><b>[[item.tipo]]</b> <br> [[item.cuenta]]: ([[item.moneda]] [[item.saldo]])  </div>
                
            </template>
         
            </vaadin-combo-box>
            <p><h5><span  class="badge badge-primary" id="selecion1"></span></h5></p>
         </div>
      </div>
      <div class="col-lg-6">
         <h4>Seleccionar Tarjeta</h4>
         <p>Cuenta de destino de fondos.</p>
         <div>
            <vaadin-combo-box    id="combo2" style="width:100%" name="combo2"   item-value-path="cuenta"  item-label-path="cuenta">
               <template>
              
                  <div><span> <iron-icon icon="vaadin:credit-card"> </iron-icon>  </span><b>[[item.tipo]]</b> <br> [[item.cuenta]]: <br><b>Linea:</b> ([[item.moneda]] [[item.linea]]) <b>Disponible:</b> ([[item.moneda]] [[item.saldo]])  </div>
                 
               </template>
            </vaadin-combo-box>
            <p><h5><span  class="badge badge-primary" id="selecion2"></span><br>
            <template is="dom-if" if="[[lineas]]"> 
            <span class="badge badge-secondary">Línea de crédito: {{lineas}} </span>
          </template>

         <template is="dom-if" if="[[saldos]]"> 
            <span class="badge badge-warning">Disponible: {{saldos}} </span>
         </template>
         </h5>
            </p>
            <iron-ajax  
           
            method="get"
            id="lstpaydaysAjax"
            handle-as="json"
            loading="{{cargando}}"
            on-response="responseLetras" last-response="responseLetras" auto on-error="handleUserError"
            >
          </iron-ajax>  

       
          <div class="d-none" id="pantalldata"> 
            <vaadin-radio-group theme="vertical"  id="radioid" >
                <template is="dom-repeat"  items="{{diasdePago}}" as="item"> 
                      <vaadin-radio-button  value="{{item}}"  disabled="{{item.disponible}}"> Cuota ({{item.letra}}): {{moneda2}} {{item.monto}},<span class="first-letter"> {{item.diapago}} </span>
                      
                      <template is="dom-if" if="[[item.vencido]]"> 
                        <span class="badge badge-danger  "> {{item.vencido}} </span>
                     </template>

                     <template is="dom-if" if="[[item.dosponible]]"> 
                     <span class="badge badge-success  "> {{item.dosponible}} </span>
                  </template>
                      
                     
                     </vaadin-radio-button> 
                </template>
                </vaadin-radio-group> 
          </div>

          <div class="d-none" id="pasofinal" > 
            <div dir="ltr">
            <vaadin-text-field id="txtmonto" label="Ingresar monto" prevent-invalid-input pattern="[0-9]*[.]*[0-9]*" style="width: 80%;">
            <div slot="prefix" ><span id="currency"></span></div>
            </vaadin-text-field>
            </div>
            
            <div>
            <vaadin-checkbox id="dalecheck" style="padding-left: 0px;"><small>Estoy de acuerdo con los <a href>Términos &amp; Condiciones</a></small></vaadin-checkbox> </p>
                
            <vaadin-button class="disable" id="btnsuccesstransfer" on-click="sendPayment"  theme="success primary">Pagar Tarjeta</vaadin-button>
        
            </div>
          </div>

          <div>
          <template is="dom-if" if="[[msgok]]"> 
           <div class="notice notice-success">
           <strong>Info: </strong> [[msgok]]
          </div> 
          </template>
          </div>
          </div>
          </div> 
     
   </div>
   <div>
             
   <vaadin-horizontal-layout> 
   <iron-ajax auto
     url="http://localhost:3000/movimiento/[[user.doi]]/[[tipotrans]]"
     method="get"
     id="listamovimientospl"
     handle-as="json"
     loading="{{cargando}}" 
     last-response="{{data}}"
     on-error="handleUserError1">
   </iron-ajax> 
   <vaadin-grid aria-label="Basic Binding Example" items="{{data.movimientos}}"> 
   
   <vaadin-grid-column width="70px" flex-grow="0"><template class="header">Número</template><template>{{item._id}}</template></vaadin-grid-column>
   <vaadin-grid-column><template class="header">Fecha y Hora de Operacion</template><template>{{item.fechahora}}</template></vaadin-grid-column>
   <vaadin-grid-column><template class="header">Canal</template><template>[[item.canal]]</template></vaadin-grid-column>
   <vaadin-grid-column><template class="header">Tipo de Operacion</template><template>[[item.operacion]]</template></vaadin-grid-column> 
   <vaadin-grid-column width="200px"><template class="header">Cuenta de Operaion</template><template>[[item.cuenta]]</template></vaadin-grid-column> 
   <vaadin-grid-column><template class="header">Moneda y Monto</template><template>[[item.monto]]</template></vaadin-grid-column> 

 </vaadin-grid>

   </vaadin-horizontal-layout> 
   </div>
</div>
<!-- /container -->

   
    `;
    } clickradio(){
        alert('clicl radio:');
    }

    sendPayment() {
        let payment = new Object({
            cuenta_cargo: this.itemseleccionado.cuenta,
            cuenta_pago: this.itemseleccionado2.cuenta,
            letra: this.$.radioid.value.letra,
            monto: this.$.txtmonto.value.replace(",",""), 
        });
        this.formData = payment; 
        this.$.registrarPago.url = WebService().SERVICIO_PAGO_TARJETA;
        this.$.registrarPago.body = this.formData;
        this.$.registrarPago.generateRequest(); 
    }

    responsePago(event) {
        var response = JSON.parse(event.detail.response);
        if (response.ok) { 
            MessageUtils().FRONT_NOTIFICATION(Message().MSG_SUCCESS);
           // Token().Set(this.$.listamovimientospl);
           // this.$.listamovimientospl.generateRequest();
           this.$.combo1.value = null;
           this.$.combo2.value = null;
           this.$.txtmonto.value="";
           this.$.btnsuccesstransfer.classList.add(StringUtils().HTML_DISABLE);
           this.$.dalecheck.classList.add(StringUtils().HTML_DISABLE);

          
           this.$.pasofinal.classList.remove("d-block");
           this.$.pasofinal.classList.add("d-none");

           this.$.pantalldata.classList.remove("d-block");
           this.$.pantalldata.classList.add("d-none");
          
            this.$.ajaxCombo2.generateRequest();  
            this.$.pantalldata.classList.remove("d-block");
            this.$.pantalldata.classList.add("d-none");
            this.$.selecion2.textContent='';
            this.$.selecion1.textContent='';


           this.$.dalecheck.checked=false;
           this.formData = {};
           this.msgok=Message().MSG_SUCCESS;
          
           Token().Set(this.$.listamovimientospl);
           
           this.$.listamovimientospl.generateRequest();
           this.lineas=null;
           this.saldos=null;

            
        }
       
    }
 
    clickradio(e){
  
      

        if(this.itemseleccionado==undefined){
            MessageUtils().FRONT_NOTIFICATION('Primero Selecione la cuenta de Cargo'); 
        }else {
            if(this.existe_vencido==true&& this.$.radioid.value.vencido==undefined){
                MessageUtils().FRONT_NOTIFICATION('Primero Selecione la cuota vencida'); 
            }else {
            const formatter = new Intl.NumberFormat(Location().EN, {
                maximumFractionDigits: 2,
                minimumFractionDigits: 2
            });
            console.info('clicl radio radioid:'+this.$.radioid.value);
             console.info('seleccionado:'+Number(this.$.radioid.value.monto));
            console.info(Number(this.itemseleccionado.saldo)<=Number(this.$.radioid.value.monto));
            if(Number(this.itemseleccionado.saldo)<=Number(this.$.radioid.value.monto)){
                MessageUtils().FRONT_NOTIFICATION("No cuenta con fondos suficientes");
                this.$.lstpaydaysAjax.generateRequest(); 
                this.$.txtmonto.value='';
    
            }else {
                this.$.txtmonto.value = formatter.format(this.$.radioid.value.monto);
                this.$.pasofinal.classList.remove("d-none");
                this.$.pasofinal.classList.add("d-block");
            }
        }
        }

        
        
    }
    iniciarcarga() { 
        alert('carga');
    } 
    
    irahome() {
        Navigation().LINK_HOME(this);
    }
    constructor() {
        super();
    }
    
    static get properties() {   
        this.user = JSON.parse(sessionStorage.getItem(WebSesion().CODE_USER_SESSION));  
        return {
            formData: {
                type: Object,
                value: {}
            },
            user: {
                value: this.user
            },
            cuenta : {
                value: ''
            },
            tipotrans: {
                value: [[TipoMovimiento().MOVPAGOSCARD]]
            }
            
        };
    } 

    _checkedButtonChanged(checkedButton) {
        alert('checked'+checkedButton);
        this._radioButtons.forEach(button => button.checked = button === checkedButton);
        if (checkedButton) {
          this.value = checkedButton.value;
        }
        this.validate();
      }

    ready() {
        super.ready();
        this.$.combo1.addEventListener(WebAction().CHANGE, e => {
            this.chagneCombo1(e)
        });
        this.$.combo2.addEventListener(WebAction().CHANGE, e => {
            this.chagneCombo2(e)
        });
        this.$.txtmonto.addEventListener(WebAction().KEYUP, e => {
            this._handleKeyUp(e)
        });
        this.$.dalecheck.addEventListener(WebAction().CHANGE, e => {
            this._handleClickCheck(e)
        });
        
        this.$.radioid.addEventListener('click', e => {
            this.clickradio(e)
        });

      
        Token().Set(this.$.listamovimientospl); 
        Token().Set(this.$.ajaxCombo1);
        Token().Set(this.$.ajaxCombo2);
        Token().Set(this.$.registrarPago);
        Token().Set(this.$.lstpaydaysAjax);

        
       
    }

    _handleKeyUp(e) {
        this.montocargo = this.$.txtmonto.value;
 
    }

    _handleClickCheck(e) { 
        console.log(this.$.txtmonto.value.indexOf(StringUtils().POINT));
        console.log(this.$.txtmonto.value);

        if (this.$.txtmonto.value.indexOf(StringUtils().POINT)== -1) {
            console.info(e.target.id + 'NO TIENE PUNTO ENTRA ');
            const formatter = new Intl.NumberFormat(Location().EN, {
                maximumFractionDigits: 2,
                minimumFractionDigits: 2
            });
            this.$.txtmonto.value = formatter.format(this.$.txtmonto.value);
        }

       
        
        console.info(e.target.id + ' was _handleClickCheck.'+ this.$.dalecheck.checked);
        if(this.$.dalecheck.checked){
            if(this.$.txtmonto.value>0){ 
            this.$.btnsuccesstransfer.classList.remove(StringUtils().HTML_DISABLE);
        }else if(this.$.txtmonto.value<=0){ 
            this.$.btnsuccesstransfer.classList.add(StringUtils().HTML_DISABLE);
            MessageUtils().FRONT_NOTIFICATION(Message().MSG_VERIFICARMONTOVALIDO);
            this.$.dalecheck.checked=false;
        }

        }else {
            this.$.btnsuccesstransfer.classList.add(StringUtils().HTML_DISABLE);
        }
    }
    chagneCombo1(event){
        this.msgok='';
        this.itemseleccionado = (this.$.combo1.items.filter(cuenta => cuenta.cuenta == this.$.combo1.value))[0]; 
        this.$.selecion1.textContent='Cuenta: '+ this.itemseleccionado.cuenta+ ':'+ this.itemseleccionado.moneda+' '+ + this.itemseleccionado.saldo; 
      
        console.info('Selecion');
        console.info('Selecion moneda: '+this.itemseleccionado.moneda);
        if (this.itemseleccionado.moneda === 'PEN') {
            this.$.currency.textContent = 'S/ ';
            this.moneda1 = 'S/ ';
        } else
        if (this.itemseleccionado.moneda === 'USD') {
            this.$.currency.textContent = '$ ';
            this.moneda1 = 'S/ ';
        } else {
            this.$.currency.textContent = '';
            this.moneda1 = '';
        }

        this.$.ajaxCombo1.generateRequest();
        this.$.ajaxCombo2.generateRequest();  
        this.$.pantalldata.classList.remove("d-block");
        this.$.pantalldata.classList.add("d-none");
        this.$.pasofinal.classList.remove("d-block");
        this.$.pasofinal.classList.add("d-none");
        this.$.selecion2.textContent='';
        //this.$.combo2.items=;

    }
    chagneCombo2(event){
        this.msgok='';
        const formatter = new Intl.NumberFormat(Location().EN, {
            maximumFractionDigits: 2
        });
        this.itemseleccionado2 = (this.$.combo2.items.filter(cuenta => cuenta.cuenta == this.$.combo2.value))[0]; 
        if(this.itemseleccionado2!=undefined || this.itemseleccionado2!=null){
        this.$.selecion2.textContent='Cuenta: '+ this.itemseleccionado2.cuenta+ ':'+ this.itemseleccionado2.moneda+' '+   formatter.format( this.itemseleccionado2.saldo);   
        this.lineas=this.itemseleccionado2.moneda+' '+this.itemseleccionado2.linea;
        this.saldos=this.itemseleccionado2.moneda+' '+this.itemseleccionado2.saldo;

        //this.$.lstpaydays.url = WebService().SERVICIO_MOVIMIENTOS;
        //this.$.lstpaydays.body = this.formData;
        this.cuenta=this.$.combo2.value;
        
        
        this.$.pantalldata.classList.remove("d-none");
        this.$.pantalldata.classList.add("d-block");
      //  this.$.pasofinal.classList.remove("d-none");
      //  this.$.pasofinal.classList.add("d-block");
       //this.$.tblcuotas.items

       if (this.itemseleccionado2.moneda === 'PEN') {
        this.$.currency.textContent = 'S/ ';
        this.moneda2 = 'S/. ';
        } else
        if (this.itemseleccionado2.moneda === 'USD') {
            this.$.currency.textContent = '$ ';
            this.moneda2 = 'S/. ';
        } else {
            this.$.currency.textContent = '';
            this.moneda2 = '';
        }

        this.$.dalecheck.checked=true;
        //OBTENER_ CUOTAS:
        this.$.lstpaydaysAjax.url="http://localhost:3000/letras/"+this.cuenta;
        this.$.lstpaydaysAjax.generateRequest(); 
    }
    }
 
    responseLetras(event) { 
        var response =  (event.detail.response); 
        //this.$.combo1.placeholder=response.cuentas.length+' Cuentas existentes';
       //  this.$.combo1.items =response.cuentas; 

       //var d1 = new Date("2015/06/17 00:00:00");
       var array1 = [];
      console.log("===================");
      var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };


      for (var item_ of response.paydays) {

        if(item_.disponible==false && item_.estado==false){

        }else {
        console.log(':::'+item_.letra+'-'+item_.diapago+' convertido::'+(new Date(item_.diapago).toLocaleDateString('es-ES',options)));

        if(new Date(item_.diapago)>new Date() && item_.disponible ==true ){
            item_.dosponible='Cuota ';
            
           // this.$.p4.classList.remove(StringUtils().HTML_DISABLE);
           // this.$.vencido+item_.letra.classList.remove('invisible');
          //  console.log(new Date(item_.diapago)+ ' es menor que '+new Date())
        }
        else if(new Date(item_.diapago)<new Date() && item_.disponible ==true ){
            item_.vencido='Vencido';
            this.existe_vencido=true;
            
           // this.$.p4.classList.remove(StringUtils().HTML_DISABLE);
           // this.$.vencido+item_.letra.classList.remove('invisible');
          //  console.log(new Date(item_.diapago)+ ' es menor que '+new Date())
        }else {
            //item_.vencido='';
          //  console.log(new Date(item_.diapago)+ ' es mayor que '+new Date())
         //   item_.estado=true;
        }

        
        item_.disponible=!item_.disponible;
        
        item_.diapago=(new Date(item_.diapago).toLocaleDateString('es-ES',options));
       
       // alert(item_);
       
        array1.push(item_);
        console.log(array1);
            }
      }
 
        console.log("===================");

         this.diasdePago=array1.reverse();

         console.log("TAMAÑO FINAL:::"+array1.length);
    }
    responseCombo1(event) { 
        var response =  (event.detail.response); 
        this.$.combo1.placeholder=response.cuentas.length+' Cuentas existentes';
         this.$.combo1.items =response.cuentas; 
    }
    responseCombo2(event) { 
        var response =  (event.detail.response); 
        this.$.combo2.placeholder=response.cuentas.length+' Cuentas existentes';
         this.$.combo2.items =response.cuentas; 
    }

    handleUserError(event) {
        HTTPUtil().CHECK_ERROR(this,event);
    }

    handleUserError1(event) { 
        HTTPUtil().CHECK_ERROR(this,event);
    }
}
window.customElements.define('app-cards', AppCard);