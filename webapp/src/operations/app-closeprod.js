// Import PolymerElement class
import {
    PolymerElement,
    html
} from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-card/paper-card.js';
import '@polymer/iron-form/iron-form.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/app-route/app-location.js';
import '@polymer/paper-fab/paper-fab.js';
import '@polymer/iron-input/iron-input.js';

import '@vaadin/vaadin-radio-button/vaadin-radio-group.js';
import '@vaadin/vaadin-text-field/vaadin-password-field.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';
import '@vaadin/vaadin-notification/vaadin-notification.js';
import '@vaadin/vaadin-checkbox/vaadin-checkbox.js';
import '@vaadin/vaadin-select/vaadin-select.js';
import '@vaadin/vaadin-button/vaadin-button.js';
import '@vaadin/vaadin-ordered-layout/vaadin-horizontal-layout.js';
import '@vaadin/vaadin-combo-box/vaadin-combo-box.js';
import '@vaadin/vaadin-grid/vaadin-grid.js';
import '@vaadin/vaadin-grid/vaadin-grid-column';
import '@vaadin/vaadin-grid/vaadin-grid-selection-column';
import '@vaadin/vaadin-icons/vaadin-icons.js';
import '@vaadin/vaadin-radio-button/vaadin-radio-button.js';
import '@vaadin/vaadin-list-box/vaadin-list-box.js';

import '../shared-bootstrap.js';
import {
    MessageUtils,
    StringUtils,
    Navigation,
    WebService,
    WebSesion,
    WebAction,
    TipoMovimiento,
    Location,
    Token,
    Message,
    HTTPUtil
} from '../util/util-constants'; 
class AppCerrarProd extends PolymerElement {
    static get template() {
        return html `
 
<style include="shared-bootstrap"> 
 .disable {
            pointer-events: none;
            opacity: 0.4;
            opacity: 0.4;
          }
          // Disable scrolling on child elements
          .disable div,
          .disable textarea {
            overflow: hidden;
          }
</style>  
<form> 
    <app-location route="{{route}}" url-space-regex="^[[rootPath]]">  </app-location> 
    <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">  </app-route>   
    <iron-ajax  url="http://localhost:3000/accounts/[[user.doi]]/LISTA001"  handle-as="json" loading="{{cargando}}" id="ajaxCombo1"  on-response="responseCombo1" auto on-error="handleUserError"></iron-ajax>
    <iron-ajax  url="http://localhost:3000/lifepoints/[[user.doi]]"  handle-as="json" loading="{{cargando}}" id="ajaxPuntos"  on-response="responsePuntos" auto on-error="handleUserError"></iron-ajax>
    <iron-ajax id="registrarPago" method="post" content-type="application/json" handle-as="text" on-response="responsePago" on-error="handleUserError"></iron-ajax>
    
    <div class="py-10"></div> 
    <div class="container">
        <div class="jumbotron">
            <h3 class="display-5">Cierre o Cancelación de Cuentas  </h3> Si cuenta con dinero en la cuenta que desea cancelar, debe seleccionar una cuenta distinta para tranferir su saldo actual.
            <p class="lead"> </p>
          
        </div>
        
    </div>

    <div class="container">
  <div class="row" id="rowcuentas"> 
  <template is="dom-repeat"  items="{{cuentapersonales}}" as="item">  
            <div class="col-sm-3">
                <div class="card mb-4 box-shadow">
                <div class="card-header">
                    <h6 class="my-0 font-weight-normal">{{item.tipo}}</h6>
                </div>
                <div class="card-body">
                    
                    <ul class="list-unstyled mt-3 mb-4">
                    <li><h7>{{item.cuenta}} </h7></li>
                    <li>Moneda: {{item.moneda}} </li>
                    <li>Saldo:  {{item.simbol}} {{item.saldo}}</li>
                   
                    </ul>
                    <vaadin-button class="" id="btnsuccesstransfer{{item.cuenta}}" on-click="cerrarPasoi"  theme="success primary">Cerrar/Cancelar</vaadin-button> 
                </div>
                </div> 
            </div>
    </template>  
  </div>

  <div class="row" id="rowcuentas2"  > 
  <div class="col-sm-6">
  <h4>Seleccionar Cuenta destino</h4>
         <p>Cuenta de destino de fondos de la cuenta actual.</p>
         <div>
            <vaadin-combo-box    id="combo1" style="width:95%" name="combo1"   item-value-path="cuenta"  item-label-path="cuenta">
            <template>
               
                <div><b>[[item.tipo]]</b> <br> [[item.cuenta]]: ([[item.moneda]] [[item.saldo]])  </div>
                
            </template>
         
            </vaadin-combo-box>
            <p><h5><span  class="badge badge-primary" id="selecion1"></span></h5></p>
         </div>
  
  </div>
  <div class="col-sm-6">

  <vaadin-checkbox id="dalecheck" style="padding-left: 0px;"><small>Estoy de acuerdo con los <a href>Términos &amp; Condiciones</a></small></vaadin-checkbox> </p>
                <p><small id="msgCerrar"></small></p>
                <vaadin-button class="disable" id="btnsuccesstransfer" on-click="sendPayment"  theme="success primary">Confirmar Operacion</vaadin-button>
            
        </div>
      
  </div>
</div>
   

    <form> 
    `;
    }


    sendPayment() {
        let payment = new Object({
            cuenta_origen: this.itemseleccionado1.cuenta,
            cuenta_destino: this.itemseleccionado.cuenta
        });
        this.formData = payment; 
        this.$.registrarPago.url = WebService().SERVICIO_CIERREPRODUCTO;
        this.$.registrarPago.body = this.formData;
        this.$.registrarPago.generateRequest(); 
    }
 
    cerrarPasoi(item){
        var obj=(item.target.id);
        console.info('Este es el iem que viene::'+obj);
        var cuenta_=obj.replace('btnsuccesstransfer',''); 
        this.itemseleccionado1 = (this.cuentapersonales.filter(cuenta => cuenta.cuenta == cuenta_))[0]; 
        console.info(this.itemseleccionado1);
        
        this.$.rowcuentas.classList.add('disable');
        this.$.rowcuentas2.classList.remove('disable');

        this.$.msgCerrar.textContent="Yo " +this.user.name  + 
        " solicito el cierre y cancelación de mi " + this.itemseleccionado1.tipo + 
        " Número: "+  this.itemseleccionado1.cuenta +'';

        
    }

   

    responseCombo1(event) { 
        var response =  (event.detail.response); 
      // this.$.combo1.placeholder=response.cuentas.length+' Cuentas existentes';
        this.cuentapersonales =response.cuentas; 
         
        console.info(this.cuentapersonales);
         var array1 = [];
         for (var item_ of this.cuentapersonales) {
             if( item_.moneda === 'PEN'){
                 item_.simbol="S/";
             }else 
             if( item_.moneda === 'USD'){
                item_.simbol="$";
            }

         }
         array1.push(item_);
         this.cuentapersonales==array1.reverse();
         console.info(this.cuentapersonales);

         this.$.combo1.placeholder=response.cuentas.length+' Cuentas existentes';
         this.$.combo1.items =response.cuentas; 
         this.$.selecion1.textContent='';
    }

    responsePago(event) {
        var response = JSON.parse(event.detail.response);
        if (response.ok) { 
            MessageUtils().FRONT_NOTIFICATION(Message().MSG_SUCCESS);
           // Token().Set(this.$.listamovimientospl);
           // this.$.listamovimientospl.generateRequest();
           this.$.combo1.value = null;   
           this.$.dalecheck.checked=false;
           this.formData = {}; 
          
         //  this.ready();

           this.$.ajaxCombo1.generateRequest(); 
           this.$.btnsuccesstransfer.classList.add(StringUtils().HTML_DISABLE);

           this.$.rowcuentas.classList.remove('disable');
           this.$.rowcuentas2.classList.add('disable');
           this.$.msgCerrar.classList.add('invisible');

            
        }
      
    }
 
    sendPoints() {
        let points = new Object({
            doi: this.user.doi,
            cuenta: this.$.combo1.value,
            monto:  this.montorq,
            points: this.pointsrq 
        });
        this.formData = points; 
        this.$.registrarPago.url = WebService().SERVICIO_PAGO_LIFEPOINTS;
        this.$.registrarPago.body = this.formData;
        this.$.registrarPago.generateRequest(); 
       
    }
 
    

    responsePuntos(event) { 
       /* var response =  (event.detail.response); 

        this.$.txtActual.value=response.lifepoint.points;
        this.$.puntosvida=Number(this.$.txtActual.value);
        this.$.txtCanjear.classList.remove('disable');
        this.$.info.textContent=response.lifepoint.points;
        
          */
    }

    handleUserError(event) {
        HTTPUtil().CHECK_ERROR(this,event);
    }
    irahome() {
        Navigation().LINK_HOME(this);
    }
    constructor() {
        super();
    }
    ready() {
       super.ready();
       this.$.rowcuentas2.classList.add('disable');
       this.$.msgCerrar.classList.add('invisible');

       this.$.combo1.addEventListener(WebAction().CHANGE, e => {
        this.chagneCombo1(e)
    });

    this.$.dalecheck.addEventListener(WebAction().CHANGE, e => {
        this._handleClickCheck(e)
    });

        /*
        this.$.txtCanjear.addEventListener(WebAction().KEYUP, e => {
           
            this._handleKeyUp(e)
        });

        this.$.dalecheck.addEventListener(WebAction().CHANGE, e => {
            this._handleClickCheck(e)
        });
        Token().Set(this.$.listamovimientospl);

       */
        

      //Token().Set(this.$.listamovimientospl);
      Token().Set(this.$.ajaxCombo1);
      Token().Set(this.$.ajaxPuntos);
      Token().Set(this.$.registrarPago);
     // Token().Set(this.$.registrarPago);
    } 

   
    _handleClickCheck(e) { 
       
        console.info(e.target.id + ' was _handleClickCheck.'+ this.$.dalecheck.checked);
        if(this.$.dalecheck.checked){
            if(this.$.combo1.value.length>0){ 
            //this.$.btnsuccesstransfer.classList.remove(StringUtils().HTML_DISABLE);
            this.$.msgCerrar.classList.remove('invisible');
            this.$.btnsuccesstransfer.classList.remove(StringUtils().HTML_DISABLE);
        }else if(this.$.combo1.value.length<=0){ 
            this.$.btnsuccesstransfer.classList.add(StringUtils().HTML_DISABLE);
            this.$.msgCerrar.classList.add('invisible');
            MessageUtils().FRONT_NOTIFICATION("Seleccioje Cuenta destino de Fondos");
            this.$.dalecheck.checked=false;
        }

        }else {
            this.$.btnsuccesstransfer.classList.add(StringUtils().HTML_DISABLE);
        }
    }
    
    chagneCombo1(event){
      
        this.itemseleccionado = (this.$.combo1.items.filter(cuenta => cuenta.cuenta == this.$.combo1.value))[0]; 
        if(this.itemseleccionado.cuenta===this.itemseleccionado1.cuenta){
            MessageUtils().FRONT_NOTIFICATION("No pueder enviar a la misma cuenta");
            this.$.combo1.value=[];
        }else {
        this.$.selecion1.textContent='Cuenta: '+ this.itemseleccionado.cuenta+ ':'+ this.itemseleccionado.moneda+' '+ + this.itemseleccionado.saldo; 
        }
      
       

    }

    _handleKeyUp(e) { 
        if(Number(this.$.txtCanjear.value)<=this.$.puntosvida){
            this.lastval=this.$.txtCanjear.value;
        var numero = this.$.txtCanjear.value ;  
        var resto = numero % this.valorcanje;   
        console.info(this.$.txtCanjear.value);
       console.info('Resto:'+resto);
       console.info('Canjear:'+(this.$.txtCanjear.value-resto));
       var canjear=this.$.txtCanjear.value-resto;
            if ( resto == 0 ){
           // alert("multiplo");
            //this.$.txtMontoSoles.textContent =(this.$.txtCanjear.value)/this.valorcanje + '.00';
            }

            this.txtPuntosCajear= canjear+' Puntos ';
            this.pointsrq=canjear;
            this.montorq=(canjear)/this.valorcanje;
            this.$.txtMontoSoles.textContent =' S/ '+ this.montorq + '.00';

            //QUITAR PUNTOS DE ARRIBA.
         this.$.txtActual.value=this.$.puntosvida-Number(canjear);

        }else if(Number(this.$.txtCanjear.value)>this.$.puntosvida){
            MessageUtils().FRONT_NOTIFICATION('Solo tienes :'+this.$.puntosvida +' Puntos vida'); 
            this.$.txtCanjear.value=this.lastval;
        }
       
    }


    
    static get properties() {
       
        this.user = JSON.parse(sessionStorage.getItem(WebSesion().CODE_USER_SESSION));
        
        return {
            formData: {
                type: Object,
                value: {}
            },
            user: {
                value: this.user
            },
            valorcanje: {
                value: 33
            },
            tipotrans: {
                value: [[TipoMovimiento().MOVCAJEPVIDA]]
            }
            
        };
    } 
}
window.customElements.define('app-closeprod', AppCerrarProd);