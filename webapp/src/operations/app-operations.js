// Import PolymerElement class
 
import {
    PolymerElement,
    html
} from '@polymer/polymer/polymer-element.js';
import '@polymer/app-route/app-location.js';
import '@polymer/iron-input/iron-input.js'; 
import '@polymer/paper-fab/paper-fab.js';
import '@polymer/paper-card/paper-card.js';
import '@polymer/paper-button/paper-button.js'; 

import '@vaadin/vaadin-icons/vaadin-icons.js';
import '@vaadin/vaadin-checkbox/vaadin-checkbox.js';
import '@vaadin/vaadin-text-field/vaadin-password-field.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';
import '@vaadin/vaadin-notification/vaadin-notification.js';
import '@vaadin/vaadin-button/vaadin-button.js'; 
import '@vaadin/vaadin-ordered-layout/vaadin-horizontal-layout.js';

import {Navigation } from '../util/util-constants.js';

class AppOperations extends PolymerElement {
    static get template() {
        return html `
 
<style include="shared-styles"> 
        .block {
            display: flex;
            align-items: center;
            justify-content: center;
            width: 100%;
            padding: 5px  5px 5px  5px; 
            
        }

        .disable {
            pointer-events: none;
            opacity: 0.4;
            opacity: 0.4;
          }
        
          .disable div,
          .disable textarea {
            overflow: hidden;
          }

        .container {
            width: 100%;
            margin: 0 auto;
            /*outline: 1px dotted rgba(0, 0, 0, 0.2);*/
            overflow: hidden;
        }

        paper-card {
            width: 99%;
            border-radius: 5px;
            border-collapse: separate;
 
        } 
        vaadin-horizontal-layout {
            margin: 0 auto;
            align-content: center;
            width: 70%;
          
            /* background-color: rgba(0, 0, 0, 0.05);*/
        } 
        .py-10 {
            height: 30px;
        } 
        #toast2 {
            --paper-toast-background-color: red;
            --paper-toast-color: white;
            paper-toast {
                width: 300px;
                margin-left: calc(50vw - 150px);
            }
        } 
        .notification {
            font: italic 24pt serif;
            background: red;
            color: #000;
            border: .25em solid #000;
        }

</style>  

 
    <app-location route="{{route}}" url-space-regex="^[[rootPath]]">
      </app-location> 
      <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">
      </app-route>  
        <div class="py-10"></div>
        <vaadin-horizontal-layout>
            <div class="block">
                <paper-card heading="" image="https://www.bbvacontinental.pe/fbin/mult/digitalizacion-secundario_tcm1105-702143.png" alt="Transferencias">
                <div class="card-content">
                    Realiza Tranferencias entre tus cuentas y terceros desde la banca por internet.
                </div>
                <div class="card-actions"> 
                    <vaadin-button on-click="operacionesgo"  theme="primary"> <iron-icon icon="vaadin:share-square"> </iron-icon>Transferencias</vaadin-button>
                    
                </div>
                </paper-card>
                </div>
                <div class="block">
                <paper-card heading="" image="https://www.bbvacontinental.pe/fbin/mult/app-banca-movil-secundario_tcm1105-635541.png" alt="Emmental">
                <div class="card-content">
                   Paga tus préstamos estes donde estés, desde tus cuentas o tarjetas de crédito. 
                </div>
                <div class="card-actions">
                    <vaadin-button on-click="operaciones_payloans" theme="primary"> <iron-icon icon="vaadin:cash"> </iron-icon> Pago de préstamo</vaadin-button>    
                </div>
                </paper-card>
                </div>
                <div class="block">
                <paper-card heading="" image="https://www.bbvacontinental.pe/fbin/mult/ahorro-vivienda-secundario_tcm1105-662772.png" alt="Emmental">
                <div class="card-content">
                   Realiza el pago de tus tarjetas en soles o dólares, ver tus pagos del mes, etc.
                </div>
                <div class="card-actions">
                    <vaadin-button on-click="operaciones_cards" theme="primary"> <iron-icon icon="vaadin:credit-card"> </iron-icon> Pago de tarjeta</vaadin-button> 
                </div>
                </paper-card>
                </div>
        </vaadin-horizontal-layout>   
        <vaadin-horizontal-layout>
        <div class="block">
                <paper-card heading=" " image="https://www.bbvacontinental.pe/fbin/mult/cuenta-sueldo-secundario_tcm1105-662467.png" alt="Emmental">
                <div class="card-content">
                <iron-icon icon="vaadin:doctor"> </iron-icon>  Realiza el pago de tus Seguros contratados, desde tus cuentas de ahorro o taretas de credito.
                </div>
                <div class="card-actions disable">
                    <vaadin-button theme="primary">  Compra de seguro</vaadin-button> 
                </div>
                </paper-card>
                </div>
                <div class="block">
                <paper-card heading="" image="https://www.bbvacontinental.pe/fbin/mult/cuentas-de-ahorro-secundario_tcm1105-679845.png" alt="Emmental">
                <div class="card-content">
                Realiza el canje de tus puntos vida a soles en tus cuentas en el BBVA
                </div>
                <div class="card-actions">
                    <vaadin-button on-click="operaciones_pvida" theme="primary"> <iron-icon icon="vaadin:dollar"> </iron-icon>Canje de puntos a cuenta</vaadin-button> 
                </div>
                </paper-card>
                </div>
                <div class="block">
                <paper-card heading=" " image="https://www.bbvacontinental.pe/fbin/mult/cajeros-automaticos-secundario_tcm1105-636005.png" alt="Emmental">
                <div class="card-content">
                 Bloquea temporalmente tus tarjeta, o solicita el cierre de tus cuentas.
                </div>
                <div class="card-actions">
                    <vaadin-button on-click="operaciones_cerrarope" theme="primary"> <iron-icon icon="vaadin:sign-out"> </iron-icon>Cierre de Cuenta, Tarjeta</vaadin-button>
                </div>
                </paper-card>
                </div>
        </vaadin-horizontal-layout>  
    `;
    }
    static get properties() {}
    ready() {
        super.ready();
    }
    operacionesgo() {
        Navigation().LINK_OPERATIONS_TRANFERS(this);
    }
    operaciones_payloans() {
        Navigation().LINK_OPERATIONS_PAYLOANS(this);
    }
    operaciones_cards() {
        Navigation().LINK_OPERATIONS_CARDS(this);
    }
    operaciones_pvida() {
        Navigation().LINK_OPERATIONS_LIFEPOINTS(this);
    }

    operaciones_cerrarope(){
        Navigation().LINK_OPERATIONS_CLOSEPROD(this);
    }
    irahome() {
        Navigation().LINK_HOME(this);
    }

}


window.customElements.define('app-operations', AppOperations);