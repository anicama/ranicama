
export const WebService = () => {
    return {
        //SERVICIO_LOGIN : 'http://45.35.12.106:8080/login',
        SERVICIO_LOGIN : 'http://localhost:3000/login',
        SERVICIO_CUENTAS : 'http://localhost:3000/accounts/' ,
        SERVICIO_MOVIMIENTOS : "http://localhost:3000/movimiento",
        SERVICIO_PAGO_PRESTAMOS : "http://localhost:3000/pagarletra",
        SERVICIO_PAGO_TARJETA : "http://localhost:3000/pagarcuota",
        SERVICIO_PAGO_LIFEPOINTS : "http://localhost:3000/lifepoints",
        SERVICIO_CIERREPRODUCTO : "http://localhost:3000/baja",

    }
}
export const WebSesion = () => {
    return {
        CODE_USER_SESSION : 'user_session', 
        CODE_TOKEN_SESSION : 'token_session', 
        SAVE_USER_SESSION : (usedata) => {
            sessionStorage.setItem(WebSesion().CODE_USER_SESSION, JSON.stringify(usedata));
        },
        SAVE_TOKEN_SESSION : (usedata) => {
            sessionStorage.setItem(WebSesion().CODE_TOKEN_SESSION, JSON.stringify(usedata));
        }

    }
}

export const Token = () => {
    return { 
        Set : (elementajax) => {
            elementajax.set( 'headers', {"token": JSON.parse(sessionStorage.getItem(WebSesion().CODE_TOKEN_SESSION)) });
        } 

    }
}


export const StringUtils = () => {
    return {
        HTML_DISABLE : 'disable' ,
        EMPTY : '',
        CURRENCY : 'currency',
        POINT : '.' ,
        RBAR : '/',
        SPACE : ' ' 
        

    }
}

export const Location = () => {
    return {
        EN : 'en-US' ,
        ES : '' 
        

    }
}

export const WebAction = () => {
    return {
        CHANGE : 'change',
        KEYUP:'keyup' 

    }
}
export const Navigation = () => {
    return {
        ALIAS_PRIVATE_ZONE:'app-privatezone',
        ALIAS_PUBLIC_ZONE:'app-publiczone',
        ALIAS_PUBLIC_NOTFOUND:'app-notfound',
        ALIAS_SITES: [ 'app-login', 'app-profile','app-publiczone','app-privatezone','app-home','app-register'],
        ALIAS_SITES_PATH: [ 'operations'],
        ALIAS_SITES_TEMP: [ 'app-operations','app-transfers','app-payloans','app-cards','app-lifepoints','app-closeprod'],
        //ALIAS_SITES_TEMP: [ 'app-operations'],
        LINK_PRIVATE_ZONE: (context) => {  context.set('route.path', '/app-privatezone');   },
        LINK_OPERATIONS_TRANFERS :(context) => {  context.set('route.path', '/app-transfers');   },
        LINK_OPERATIONS_OPERATIONS :(context) => {  context.set('route.path', '/app-operations');   },
        LINK_OPERATIONS_PAYLOANS :(context) => {  context.set('route.path', '/app-payloans');   },
        LINK_OPERATIONS_LIFEPOINTS :(context) => {  context.set('route.path', '/app-lifepoints');   },
        LINK_OPERATIONS_CLOSEPROD :(context) => {  context.set('route.path', '/app-closeprod');   },
        LINK_OPERATIONS_CARDS :(context) => {  context.set('route.path', '/app-cards');   },
        LINK_HOME: (context) => {  context.set('route.path', '');   }
        

    }
}

export const MessageUtils = () => {
    return {
        FRONT_NOTIFICATION: (message) => {
            var position = 'top-center',
                duration = 5000;
            const notify = window.document.createElement('vaadin-notification');
            notify.renderer = function (root) {
                root.textContent = message;
            };
            window.document.body.appendChild(notify);
            notify.id = "notification";
            notify.className = "notification";
            notify.position = position;
            notify.duration = duration;
            notify.opened = true;
            notify.addEventListener('opened-changed', function () {
                window.document.body.removeChild(notify);
            });
        
        
        
        },
        FRONT_NOTIFICATION2: (message,time) => {
            var position = 'top-center',
                duration = time;
            const notify = window.document.createElement('vaadin-notification');
            notify.renderer = function (root) {
                root.textContent = message;
            };
            window.document.body.appendChild(notify);
            notify.id = "notification";
            notify.className = "notification";
            notify.position = position;
            notify.duration = duration;
            notify.opened = true;
            notify.addEventListener('opened-changed', function () {
                window.document.body.removeChild(notify);
            });
        
        
        
        } 

    }
}

export const HTTPUtil = () => {
    return {
         CHECK_ERROR: (context,event) => {
            console.info(event.detail.request.xhr.status);
            if (event.detail.request.xhr.status==401){
                MessageUtils().FRONT_NOTIFICATION("Sesion expirada, Por favor inicie sesión nuevamente",5000);
            }else if(event.detail.request.xhr.status==0){
                MessageUtils().FRONT_NOTIFICATION2("No Existe Conexión con el Servicio",5000);
            }else {
                if(event.detail.request.xhr.response!=null){ 
                    context.error = JSON.parse(event.detail.request.xhr.response).err.message; 
                    context.status = JSON.parse(event.detail.request.xhr.response).err.invalid; 
                    MessageUtils().FRONT_NOTIFICATION(context.error,5000);
                    }else {
                        MessageUtils().FRONT_NOTIFICATION("Error desconocido, Intente nuevamente",5000);
                    }
            }
        
        },


    }
}


export const Message = () => {
    return {
        MSG_EXEDE_MONTO : 'Hey estas excediendo del monto',
        MSG_CUENTAS_IGUALES : 'Hey las cuentas son iguale' ,
        MSG_EXCEDE_MONTO : "estas excediendo del monto de tu cuenta: ",
        MSG_SUCCESS:"Transacción realizada",
        MSG_VERIFICARMONTOVALIDO:"Ingresar monto válido"

    }
}



export const TipoMovimiento = () => {
    return {
        MOVTRANSFERS : 'TRANSF001',
        MOVPRESTAMOS : 'PRESTA001' ,
        MOVPAGOSCUOT : 'PREST001',
        MOVPAGOSCARD : 'CARD001',
        MOVCAJEPVIDA : 'CANJE001'

    }
}

 