// Import PolymerElement class

import {  PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '@polymer/iron-input/iron-input.js'; 
import '@polymer/paper-button/paper-button.js'; 
import '@polymer/app-route/app-location.js';
import '@vaadin/vaadin-checkbox/vaadin-checkbox.js';
import '@vaadin/vaadin-button/vaadin-button.js'; 
import '@vaadin/vaadin-icons/vaadin-icons.js';
import '@polymer/paper-fab/paper-fab.js';
import '@vaadin/vaadin-text-field/vaadin-password-field.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';
import '@vaadin/vaadin-notification/vaadin-notification.js';
import '@vaadin/vaadin-ordered-layout/vaadin-horizontal-layout.js';
import '@vaadin/vaadin-form-layout/vaadin-form-layout.js';
import '@polymer/paper-card/paper-card.js';
import './shared-styles.js';
import './shared-stylesalt.js'; 
import {MessageUtils, Navigation, WebSesion, WebService,HTTPUtil } from  './util/util-constants.js';



// define the element's class element
class AppLogin extends PolymerElement {
    static get template() {
            return html `  
 <app-location route="{{route}}" url-space-regex="^[[rootPath]]">   </app-location><app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}"> </app-route>
 <iron-ajax id="registerLoginAjax" method="post" content-type="application/json" handle-as="text" on-response="handleUserResponse" on-error="handleUserError"></iron-ajax> 
        <main>
        <div id="primary" class="blue4 p-t-b-100 height-full responsive-phone">
           <div class="container">
              <div class="row">
              <div class="col-lg-3 p-t-100">
    </div>
                 <div class="col-lg-6 p-t-100">

                 <p class=" p-t-b-20 font-weight-lighter"> 
                       <center>  <h1 class="s-48 text-white">BBVA Banco Continental</h1><center>
                       </p>
                  
                    <div class="text-white">
 
                       <p class="s-18 p-t-b-20 font-weight-lighter"> 
                          Por favor ingrese sus credenciales de acceso a la zona privada del BBVA 
                       </p>
                    </div>
                    <form  >
                       <div class="row">
                          <div class="col-lg-6">
                             <div class="form-group has-icon ">
                                <vaadin-text-field class="form-control custom-style no-b"  id="email"  value="{{formData.email}}"  placeholder="Enter email">
                                   <iron-icon icon="vaadin:user" slot="prefix"></iron-icon>
                                </vaadin-text-field>
                             </div>
                          </div>
                          <div class="col-lg-6">
                             <div class="form-group has-icon ">
                                <vaadin-password-field  class="form-control form-control-xl no-b" id="password" placeholder="Enter password" value="{{formData.password}}">
                                   <iron-icon icon="vaadin:password" slot="prefix"></iron-icon>
                                </vaadin-password-field>
                             </div>
                          </div>
                          <div class="col-lg-12">
                             <vaadin-button id="btnlogin"  class="btn btn-success btn-lg btn-block"  on-click="iniciarSesionFunction"  theme="success primary">Iniciar Sesion</vaadin-button>
                             <p class="forget-pass text-white">Have you forgot your username or password ?</p>
                             <vaadin-button id="btnlogininicio" theme="contrast primary" on-click="homeFunction" theme="tertiary-inline">Regresar</vaadin-button>
                          </div>
                       </div>
                    </form>
                 </div>
                 <div class="col-lg-3 p-t-100">
                    
                 </div>
              </div>
              
           </div>
        </div>
     </main>
                 
 <style include="shared-stylesalt">
     .form-control {
     display: flex;
     }
     vaadin-text-field{
     background-color: #FFFFFF;
     }
     .py-10 {
     height: 30px;
     }
     .jumbotron {
     background-color: #e5f0f9 !important;
     border-color: #004b8c !important;
     } 
  </style>
        `;
        } 
     
    ready() {
        super.ready(); 
    } 
    static get properties() { 
        this.error = '';
        return {
              formData: {
                type: Object,
                value: {}
            } 
        }; 
    } 
   
    iniciarSesionFunction() {
        this.$.registerLoginAjax.url = WebService().SERVICIO_LOGIN;
        this.$.registerLoginAjax.body = this.formData;
        this.$.registerLoginAjax.generateRequest();
    } 

    handleUserResponse(event) {
        var response = JSON.parse(event.detail.response);  
        if (response.token) {
            this.error = ''; 
            this.storedUser = {
                role: response.usuario.role,
                doi: response.usuario.doi,
                estado: response.usuario.estado,
                google: response.usuario.google,
                id: response.usuario._id,
                name: response.usuario.nombre,
                nombre: response.usuario.nombre,
                nacimiento: response.usuario.nacimiento,
                direccion: response.usuario.direccion,
                tel1: response.usuario.tel1,
                tel2: response.usuario.tel2,
                tel3: response.usuario.tel3,
                email: response.usuario.email,
                token: response.token,
                loggedin: response.ok
            };  
            WebSesion().SAVE_USER_SESSION(this.storedUser); 
            WebSesion().SAVE_TOKEN_SESSION(response.token); 
            //alert(this.storedUser);
           // Navigation().LINK_PRIVATE_ZONE(this);
           Navigation().LINK_OPERATIONS_OPERATIONS(this);

         
        } 
        this.formData = {};
    } 
    homeFunction() {
       Navigation().LINK_HOME(this); 
    }

    handleUserError(event) { 
      HTTPUtil().CHECK_ERROR(this,event);
    }  
}

 
customElements.define('app-login', AppLogin);